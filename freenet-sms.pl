#!/usr/bin/perl -w
#
#        freenet-sms v0.3 - A tool for sending SMS-messages to mobile phones
#        Copyright (C) 2000 Andreas Gutowski <guddl@guddl.de>
#
#        This program is free software; you can redistribute it and/or
#        modify it under the terms of the GNU General Public License as
#        published by the Free Software Foundation; either version 2 of
#        the License, or (at your option) any later version.
#
#        This program is distributed in the hope that it will be useful,
#        but WITHOUT ANY WARRANTY; without even the implied warranty of
#        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#        General Public License for more details.
#
#        You should have received a copy of the GNU General Public License
#        along with this program; if not, write to the Free Software
#        Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
#

# ** Modules **
use HTTP::Response;
use HTTP::Status;

#
## choose your debuglevel
#
my $debuglevel = 1;    # Debuglevel (1..5)


package RequestAgent;
no strict;
use LWP::UserAgent;             # libwww-perl

($proxy_username,$proxy_password,$last_redirect) = (undef,undef,undef);

@ISA = qw(LWP::UserAgent);

sub new {
    $self = LWP::UserAgent::new(@_);
    $self->agent("smsweb");
    $self;
}

sub get_basic_credentials {
    my $self = shift;
    return ($proxy_username,$proxy_password);
}

sub redirect_ok {
    my($self, $request) = @_;
    $last_redirect=$request->uri;
    return TRUE;
}

package freenetsms;
use strict; 
use Getopt::Long;


# ** Definitions **

# URL of freenet login
my $url_loginpage = "http://e-tools.freenet.de//freenetLogin.php3";  

# URL of freenet sendpage
my $url_sendpage  = "http://e-tools.freenet.de/sms.php3";

my %config;
my $home                = $ENV{"HOME"};
my $silent              = undef;
my $smsnumber;
my $smsmessage;
my $vorwahl             = undef;
my $telnummer           = undef;

my $sessionid;
my $messageid;
my $messagelen;

# ** LWP Definitions **
my $agent       = RequestAgent->new;  # Create an UserAgent object
my $request     = HTTP::Request->new;
my $response;

#
# ** some funktions **
#

# function to display an errormessage
#
sub fatalerror {
    my ($id,$text,$res) = @_;
    print "Error: [$id] $text\n";
    print "\n".$res->error_as_HTML if (($res) && ($debuglevel>1));
    exit;
}

# function to display a usage-message
#
sub usage {
    my ($reason) = @_;
    if ($reason) { print "Syntax error: $reason\n"; }
    print "Usage: \n\t$0 <smsnumber> <message>";
    exit;
}


# function to read the configfile .freenet-smsrc 
#
sub read_configfile {

    open(CONFIGF, "$home/.freenet-smsrc") || fatalerror("config","Could not open configfile. Check ~/.freenet-smsrc.");

    my $key;
    my $number;
    my $line;

    while (<CONFIGF>) {
	
	$line = $_;
        chomp $line;

        if ((!(/^#/)) && (/=/))  {

              print "CONFIG: $line\n" if ($debuglevel==5);

              ($key,$number) = (split(/=/,$line));
	      $config{$key} = $number;
	     	      
        };

    };
	   
    close (CONFIGF) || fatalerror("config","Couldn't close configfile. Check ~/.reenet-smsrc");

} # end read_configfile

# function to get the commandlineoptions
#
sub get_cmdlineoptions {
    # Check Command-Line Parmaters
    GetOptions("debug=i",\$debuglevel,"silent",\$silent);
    $debuglevel=5 if ($debuglevel >5);
    $debuglevel=1 if ($debuglevel <1);

    # Check ARGV count
    &usage unless ($#ARGV > 0);
    $smsnumber = $ARGV[0];

    # Valid SMS-Number (100% digits) or valid alias given ?
    if (($smsnumber =~ /\D/) && (! $config{$smsnumber})) {
	&usage("Invalid SMS-Number/Alias \"$smsnumber\" !\n\n");
    }

    # If an alias if given -> get number
    if ($config{$smsnumber}) {
	$smsnumber = $config{$smsnumber};
    };

    # spilt areacode from telefonnumber
    $vorwahl    = substr($smsnumber,0,4);
    $telnummer  = substr($smsnumber,4,10);

    shift(@ARGV);
    $smsmessage = join(" ",@ARGV);

    $messagelen = length $smsmessage;

    print "Tel.-Nummer: $smsnumber \nMessage    : $smsmessage\nTextlänge  : $messagelen\n\n" if ($debuglevel==5);

    if ($messagelen > 120) {
	fatalerror("send","Message to long max. 120 characters!");	
    };
}

 

# ** Login **
#
sub login {

    print "Logging in... \n" if ($debuglevel>1);

    # Prepare Request (POST)
    $request->method("POST");
    $request->uri($url_loginpage);

    # Insert Form-Data
    $request->content_type('application/x-www-form-urlencoded');
    $request->content("callback=http://etools.freenet.de/etools.php3&password=".$config{"password"}."&username=".$config{"username"});

    # Do the Request
    $response = $agent->request($request);
    
    # Request succsesfull ?
    if ($response->is_success) {
	print "- Loaded sucessfully: $url_loginpage \n" if ($debuglevel>3);
    } else {
	fatalerror("login","Couldn't load $url_loginpage",$response);
    }

    # Session ID given ?
    if ($response->is_success) {
        # Filter out Sessionid
	$sessionid = $response->previous->headers->header("Location");
	$sessionid = (split(/SIS_param=/,$sessionid))[1];
	$sessionid =~ s/&.*&//g;
    } else {
	fatalerror("login","Did not get a Session-ID, login incorrect ?");
    }

        print "Got Session-ID: $sessionid\n" if ($debuglevel >2);

        print "Login successfull !\n" if ($debuglevel>1);


}; # end of function login

sub sendsms  {
	
	# SMS-Sendeseite aufrufen
	
	my $url = "$url_sendpage"."?SIS_param=$sessionid";

	$request->method("POST");
	$request->uri($url);

	#my $formdata = sprintf($send_formvals,$sessionid,$smsnumber,$message);
 
	# Menüseite aufrufen

	$url = "$url_sendpage"."?SIS_param=$sessionid";
	
	#print "To send: $formdata";

	$request->method("POST");
	$request->uri($url);

	# SMS versenden
	
	$url = "$url_sendpage"."?SIS_param=$sessionid&aktion=sms_write&zielrufnummer=&anrede=Herr&nachname=";
	
	$request->method("POST");
	$request->uri($url);

	$request->content_type('application/x-www-form-urlencoded');
	$request->content("SIS_param=$sessionid&nachname=&vorname=&vorwahl=$vorwahl&zielrufnummer=$telnummer&smstext=$smsmessage&aktion=Senden");

	$response = $agent->request($request);

	if ($response->is_success) {
		print "- Loaded sucessfully:$url_sendpage\n" if ($debuglevel>3);
		print "- SMS successfully sent to $smsnumber.\n\n";
	} else {
		fatalerror("send","Couldn't load $url_sendpage. SMS not sent.",$response);
	}

	# To see the HTML output of the resultpage
        # uncomment the following line. 
	#print $response->as_string;

} # end of function sendsms

&read_configfile;
&get_cmdlineoptions;

if ($config{"http_proxy"}) {
        $agent->proxy("http",$config{http_proxy});
        $RequestAgent::proxy_username=$config{"http_proxy_username"};
        $RequestAgent::proxy_password=$config{"http_proxy_password"};
}

print "VW: $vorwahl\nTN: $telnummer\n\n" if ($debuglevel > 4);

&login;
&sendsms;
