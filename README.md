### Das Programm funktioniert schon seit … keine Ahnung wie lange nicht mehr. Aber zum anschauen, umbauen usw. ist es ja noch geeignet.


Freenet SMS


freenet-sms (deutsch)

freenet-sms ist ein kleines Perl-Script zum versenden von SMS-Nachrichten. Es versendet die SMS-Nachrichten über den kostenlosen Dienst von etools.freenet.de

freenet-sms steht unter der GNU Public License


freenet-sms (englisch)

freenet-sms is a small Perl-Script for sending SMS-messages. It sends the SMS-messages via the free service of etools.freenet.de

freenet-sms released under the GNU Public License
